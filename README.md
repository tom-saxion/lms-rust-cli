# LMS Rust client

This isn't the official client for lms. 

[lms](https://gitlab.com/saxion.nl/42/lms42)


### Features
 - [X] Upload work
 - [X] Download work
 - [X] Download template
 - [X] Reorder file structure
 - [X] Grade work
 - [X] Login
 - [X] Open work dir
 - [ ] Handle setup
 
### Extra features
 - [X] Change default editor
 - [X] Open work offline 
 - [X] Download all assignments 
 - [X] Check for todos in your code before upload for "sql", "rs", "py", "js", "css", "html", "svelte"
 - [ ] Add set command for config chages 
 - [ ] Add get command to verify config changes 
 - [ ] Add lms check to check TODO in code

Avalible for `arm` and `x86` systems
